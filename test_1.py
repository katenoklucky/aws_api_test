__author__ = 'edanilova'

# Create VPC with 2 instances, each instance have access into Internet and on other instance

from creator_script import creator

CidrBlock1 = '172.33.0.0/16'
CidrBlock2 = '172.33.0.0/24'
CidrBlock3 = '172.33.1.0/24'
CidrBlock4 = '172.32.0.0/16'
AvailabilityZone = 'us-west-2a'
# vpc_id1 = 'vpc-d8b0f9bd'
key_name = 'my_key'

# key_name = creator('key', {'KeyName': 'key1'})

vpc_id1 = creator('vpc', {'CidrBlock': CidrBlock1})
# vpc_id2 = creator('vpc', {'CidrBlock': CidrBlock4})

# vpc_peering_connection_id = creator('vpc_peering_connection', {'VpcId': vpc_id1, 'PeerVpcId': vpc_id2})

allocate_address = creator('allocate_address', {'Domain': 'vpc'})
public_ip = allocate_address[0]
allocation_id = allocate_address[1]

allocate_address2 = creator('allocate_address', {'Domain': vpc_id1})
public_ip2 = allocate_address2[0]
allocation_id2 = allocate_address2[1]

internet_gateway_id = creator('internet_gateway', {})
attach_to_vpc = creator('internet_gateway_attach_to_vpc', {'Internet_Gateway_ID': internet_gateway_id, 'VpcId': vpc_id1})

security_group_id = creator('security_group', {'GroupName': 'Security Group Test', 'Description': 'Description', 'VpcId': vpc_id1})
authorize_security_group_ingress_tcp = creator('authorize_security_group_ingress', {'GroupId': security_group_id,
                                                                    'IpPermissions': [{'IpProtocol': 'tcp',
                                                                    'FromPort': 22,
                                                                    'ToPort': 22,
                                                                    'IpRanges': [{'CidrIp': '0.0.0.0/0'}]}]})

authorize_security_group_egress_tcp = creator('authorize_security_group_egress', {'GroupId': security_group_id,
                                                                    'IpPermissions': [{'IpProtocol': 'tcp',
                                                                    'FromPort': 22,
                                                                    'ToPort': 22,
                                                                    'IpRanges': [{'CidrIp': '0.0.0.0/0'}]}]})

authorize_security_group_ingress_icmp = creator('authorize_security_group_ingress', {'GroupId': security_group_id,
                                                                    'IpPermissions': [{'IpProtocol': 'icmp',
                                                                    'FromPort': -1,
                                                                    'ToPort': -1,
                                                                    'IpRanges': [{'CidrIp': '0.0.0.0/0'}]}]})

authorize_security_group_egress_icmp = creator('authorize_security_group_egress', {'GroupId': security_group_id,
                                                                    'IpPermissions': [{'IpProtocol': 'icmp',
                                                                    'FromPort': -1,
                                                                    'ToPort': -1,
                                                                    'IpRanges': [{'CidrIp': '0.0.0.0/0'}]}]})

subnet_id1 = creator('subnet', {'VpcId': vpc_id1, 'CidrBlock': CidrBlock2, 'AvailabilityZone': AvailabilityZone})
subnet_id2 = creator('subnet', {'VpcId': vpc_id1, 'CidrBlock': CidrBlock3, 'AvailabilityZone': AvailabilityZone})

route_table_id = creator('route_table', {'VpcId': vpc_id1})
associate_route_table_id1 = creator('associate_route_table', {'SubnetId': subnet_id1, 'RouteTableId': route_table_id})
associate_route_table_id2 = creator('associate_route_table', {'SubnetId': subnet_id2, 'RouteTableId': route_table_id})

network_interface_id1 = creator('network_interface', {'SubnetId': subnet_id1})
network_interface_id2 = creator('network_interface', {'SubnetId': subnet_id2})

instance1_id = creator('instance', {'ImageId': 'ami-5189a661', 'MinCount': 1, 'MaxCount': 1, 'KeyName': key_name,
                                    'SecurityGroupIds': security_group_id, 'InstanceType': 't2.micro', 'SubnetId': subnet_id1,
                                    'NetworkInterfaces': {'NetworkInterfaceId': network_interface_id1, 'SubnetId': subnet_id1}})

associate_address_to_instance_id = creator('associate_address', {'InstanceId': instance1_id, 'AllocationId': allocation_id, 'NetworkInterfaceId': None})
# associate_address_to_network_interface_id = creator('associate_address', {'InstanceId': None, 'AllocationId': allocation_id, 'NetworkInterfaceId': network_interface_id1})

route1 = creator('route', {'RouteTableId': route_table_id, 'DestinationCidrBlock': '0.0.0.0/0', 'GatewayId': internet_gateway_id})
route2 = creator('route', {'RouteTableId': route_table_id, 'DestinationCidrBlock': '172.34.0.0/16', 'NetworkInterfaceId': network_interface_id1})
route3 = creator('route', {'RouteTableId': route_table_id, 'DestinationCidrBlock': '172.32.0.0/16', 'InstanceId': instance1_id})

instance2_id = creator('instance', {'ImageId': 'ami-5189a661', 'MinCount': 1, 'MaxCount': 1, 'KeyName': key_name,
                                    'SecurityGroupIds': security_group_id, 'InstanceType': 't2.micro', 'SubnetId': subnet_id2,
                                    'NetworkInterfaces': {'NetworkInterfaceId': network_interface_id2, 'SubnetId': subnet_id2}})

associate_address_to_instance_id2 = creator('associate_address', {'InstanceId': instance2_id, 'AllocationId': allocation_id2, 'NetworkInterfaceId': None})
# associate_address_to_network_interface_id2 = creator('associate_address', {'InstanceId': None, 'AllocationId': allocation_id2, 'NetworkInterfaceId': network_interface_id2})

associate_address_to_instance_id2 = creator('associate_address', {'InstanceId': instance2_id, 'AllocationId': allocation_id2, 'NetworkInterfaceId': None})
# associate_address_to_network_interface_id2 = creator('associate_address', {'InstanceId': None, 'AllocationId': allocation_id2, 'NetworkInterfaceId': network_interface_id2})

route1 = creator('route', {'RouteTableId': route_table_id, 'DestinationCidrBlock': '0.0.0.0/0', 'GatewayId': internet_gateway_id})
route2 = creator('route', {'RouteTableId': route_table_id, 'DestinationCidrBlock': '172.35.0.0/16', 'NetworkInterfaceId': network_interface_id2})
route3 = creator('route', {'RouteTableId': route_table_id, 'DestinationCidrBlock': '172.36.0.0/16', 'InstanceId': instance2_id})

# attach_network_interface_id_1 = creator('attach_network_interface', {'NetworkInterfaceId': network_interface_id1, 'InstanceId': instance1_id, 'DeviceIndex': 1})
# attach_network_interface_id_2 = creator('attach_network_interface', {'NetworkInterfaceId': network_interface_id2, 'InstanceId': instance2_id, 'DeviceIndex': 1})






# volume_id1 = creator('volume', {'Size': 1, 'AvailabilityZone': AvailabilityZone, 'VolumeType': 'standard', 'Encrypted': False})
# volume_id2 = creator('volume', {'Size': 1, 'AvailabilityZone': AvailabilityZone, 'VolumeType': 'standard', 'Encrypted': False})
# volume_id3 = creator('volume', {'Size': 1, 'AvailabilityZone': AvailabilityZone, 'VolumeType': 'standard', 'Encrypted': False})
# #
# volume_snapshot1 = creator('volume_snapshot', {'VolumeId': volume_id1})
# volume_snapshot2 = creator('volume_snapshot', {'VolumeId': volume_id2})
#
# attach_volume_id1 = creator('attach_volume', {'VolumeId': volume_id1, 'InstanceId': instance1_id, 'Device': '/dev/sdh'})
# attach_volume_id2 = creator('attach_volume', {'VolumeId': volume_id2, 'InstanceId': instance1_id, 'Device': '/dev/sdg'})
# attach_volume_id3 = creator('attach_volume', {'VolumeId': volume_id3, 'InstanceId': instance2_id, 'Device': '/dev/sde'})
#
# volume_from_snapshot_id1 = creator('volume_from_snapshot', {'SnapshotId': volume_snapshot1, 'AvailabilityZone': AvailabilityZone, 'VolumeType': 'standard', 'Encrypted': False})
# volume_from_snapshot_id2 = creator('volume_from_snapshot', {'SnapshotId': volume_snapshot2, 'AvailabilityZone': AvailabilityZone, 'VolumeType': 'standard', 'Encrypted': False})