__author__ = 'edanilova'

from creator_script import creator

CidrBlock1 = '172.33.0.0/16'
CidrBlock2 = '172.33.0.0/28'
CidrBlock3 = '172.33.0.16/28'
CidrBlock4 = '172.33.0.32/28'
CidrBlock5 = '172.33.0.48/28'
AvailabilityZone = 'us-west-2a'
vpc_id1 = 'vpc-4bbff42e'
key_name = 'ed_key'

# vpc_id1 = creator('vpc', {'CidrBlock': CidrBlock1})

allocate_address_i1 = creator('allocate_address', {'Domain': 'vpc'})
public_ip_i1 = allocate_address_i1[0]
allocation_id_i1 = allocate_address_i1[1]

# allocate_address_i2 = creator('allocate_address', {'Domain': 'vpc'})
# public_ip_i2 = allocate_address_i2[0]
# allocation_id_i2 = allocate_address_i2[1]

customer_gateway_id = creator('customer_gateway', {'Type': 'ipsec.1', 'PublicIp': public_ip_i1, 'BgpAsn': 65000})
vpn_gateway_id = creator('vpn_gateway', {'Type': 'ipsec.1', 'AvailabilityZone': AvailabilityZone})
vpn_connection_id = creator('vpn_connection', {'Type': 'ipsec.1', 'CustomerGatewayId': customer_gateway_id, 'VpnGatewayId': vpn_gateway_id, 'Options': {'StaticRoutesOnly': True}})
attach_vpn_gateway = creator('attach_vpn_gateway', {'VpnGatewayId': vpn_gateway_id, 'VpcId': vpc_id1})
vpn_connection_route = creator('vpn_connection_route', {'VpnConnectionId': vpn_connection_id, 'DestinationCidrBlock': CidrBlock1})

internet_gateway_id = creator('internet_gateway', {})

security_group_id = creator('security_group', {'GroupName': 'Security Group Test', 'Description': 'Description', 'VpcId': vpc_id1})

authorize_security_group_ingress_tcp = creator('authorize_security_group_ingress', {'GroupId': security_group_id,
                                                                    'IpPermissions': [{'IpProtocol': 'tcp',
                                                                    'FromPort': 22,
                                                                    'ToPort': 22,
                                                                    'IpRanges': [{'CidrIp': '0.0.0.0/0'}]}]})

authorize_security_group_egress_tcp = creator('authorize_security_group_egress', {'GroupId': security_group_id,
                                                                    'IpPermissions': [{'IpProtocol': 'tcp',
                                                                    'FromPort': 22,
                                                                    'ToPort': 22,
                                                                    'IpRanges': [{'CidrIp': '0.0.0.0/0'}]}]})

authorize_security_group_ingress_icmp = creator('authorize_security_group_ingress', {'GroupId': security_group_id,
                                                                    'IpPermissions': [{'IpProtocol': 'icmp',
                                                                    'FromPort': -1,
                                                                    'ToPort': -1,
                                                                    'IpRanges': [{'CidrIp': '0.0.0.0/0'}]}]})

authorize_security_group_egress_icmp = creator('authorize_security_group_egress', {'GroupId': security_group_id,
                                                                    'IpPermissions': [{'IpProtocol': 'icmp',
                                                                    'FromPort': -1,
                                                                    'ToPort': -1,
                                                                    'IpRanges': [{'CidrIp': '0.0.0.0/0'}]}]})

internet_gateway_attach_to_vpc = creator('internet_gateway_attach_to_vpc', {'Internet_Gateway_ID': internet_gateway_id, 'VpcId': vpc_id1})

subnet_id1 = creator('subnet', {'VpcId': vpc_id1, 'CidrBlock': CidrBlock2, 'AvailabilityZone': AvailabilityZone})
subnet_id2 = creator('subnet', {'VpcId': vpc_id1, 'CidrBlock': CidrBlock3, 'AvailabilityZone': AvailabilityZone})

route_table_id = creator('route_table', {'VpcId': vpc_id1})
route_table_id1 = creator('route_table', {'VpcId': vpc_id1})
associate_route_table_id_i1 = creator('associate_route_table', {'SubnetId': subnet_id1, 'RouteTableId': route_table_id})
associate_route_table_id_i2 = creator('associate_route_table', {'SubnetId': subnet_id2, 'RouteTableId': route_table_id1})

instance1_id = creator('instance', {'ImageId': 'ami-5189a661', 'MinCount': 1, 'MaxCount': 3, 'KeyName': key_name,
                                    'SecurityGroupIds': security_group_id, 'InstanceType': 't2.micro', 'SubnetId': subnet_id1})
associate_address_to_instance_id = creator('associate_address', {'InstanceId': instance1_id, 'AllocationId': allocation_id_i1, 'NetworkInterfaceId': None})
# route1 = creator('route', {'RouteTableId': route_table_id, 'DestinationCidrBlock': '0.0.0.0/0', 'GatewayId': internet_gateway_id})
# route3 = creator('route', {'RouteTableId': route_table_id, 'DestinationCidrBlock': '172.32.0.0/16', 'InstanceId': instance1_id})

# instance2_id = creator('instance', {'ImageId': 'ami-5189a661', 'MinCount': 1, 'MaxCount': 1, 'KeyName': key_name,
#                                     'SecurityGroupIds': security_group_id, 'InstanceType': 't2.micro', 'SubnetId': subnet_id1})
# associate_address_to_instance_id = creator('associate_address', {'InstanceId': instance2_id, 'AllocationId': allocation_id_i1, 'NetworkInterfaceId': None})
# # route1 = creator('route', {'RouteTableId': route_table_id, 'DestinationCidrBlock': '0.0.0.0/0', 'GatewayId': internet_gateway_id})
# # route3 = creator('route', {'RouteTableId': route_table_id, 'DestinationCidrBlock': '172.32.0.0/16', 'InstanceId': instance2_id})
#
# instance3_id = creator('instance', {'ImageId': 'ami-5189a661', 'MinCount': 1, 'MaxCount': 1, 'KeyName': key_name,
#                                     'SecurityGroupIds': security_group_id, 'InstanceType': 't2.micro', 'SubnetId': subnet_id1})
# associate_address_to_instance_id = creator('associate_address', {'InstanceId': instance3_id, 'AllocationId': allocation_id_i1, 'NetworkInterfaceId': None})
# # route1 = creator('route', {'RouteTableId': route_table_id, 'DestinationCidrBlock': '0.0.0.0/0', 'GatewayId': internet_gateway_id})
# # route3 = creator('route', {'RouteTableId': route_table_id, 'DestinationCidrBlock': '172.32.0.0/16', 'InstanceId': instance3_id})
#
# instance4_id = creator('instance', {'ImageId': 'ami-5189a661', 'MinCount': 1, 'MaxCount': 1, 'KeyName': key_name,
#                                     'SecurityGroupIds': security_group_id, 'InstanceType': 't2.micro', 'SubnetId': subnet_id2})
# associate_address_to_instance_id2 = creator('associate_address', {'InstanceId': instance4_id, 'AllocationId': allocation_id_i1, 'NetworkInterfaceId': None})
# # route1 = creator('route', {'RouteTableId': route_table_id1, 'DestinationCidrBlock': '0.0.0.0/0', 'GatewayId': internet_gateway_id})
# # route3 = creator('route', {'RouteTableId': route_table_id1, 'DestinationCidrBlock': '172.36.0.0/16', 'InstanceId': instance4_id})
#
# instance5_id = creator('instance', {'ImageId': 'ami-5189a661', 'MinCount': 1, 'MaxCount': 1, 'KeyName': key_name,
#                                     'SecurityGroupIds': security_group_id, 'InstanceType': 't2.micro', 'SubnetId': subnet_id2})
# associate_address_to_instance_id2 = creator('associate_address', {'InstanceId': instance5_id, 'AllocationId': allocation_id_i1, 'NetworkInterfaceId': None})
# route1 = creator('route', {'RouteTableId': route_table_id1, 'DestinationCidrBlock': '0.0.0.0/0', 'GatewayId': internet_gateway_id})
# route3 = creator('route', {'RouteTableId': route_table_id1, 'DestinationCidrBlock': '172.36.0.0/16', 'InstanceId': instance5_id})


# volume_id1 = creator('volume', {'Size': 1, 'AvailabilityZone': AvailabilityZone, 'VolumeType': 'standard', 'Encrypted': False})
# volume_id2 = creator('volume', {'Size': 1, 'AvailabilityZone': AvailabilityZone, 'VolumeType': 'standard', 'Encrypted': False})
# volume_id3 = creator('volume', {'Size': 1, 'AvailabilityZone': AvailabilityZone, 'VolumeType': 'standard', 'Encrypted': False})
#
# volume_snapshot1 = creator('volume_snapshot', {'VolumeId': volume_id1})
# volume_snapshot2 = creator('volume_snapshot', {'VolumeId': volume_id2})
#
# attach_volume_id1 = creator('attach_volume', {'VolumeId': volume_id1, 'InstanceId': instance1_id, 'Device': '/dev/sdh'})
# attach_volume_id2 = creator('attach_volume', {'VolumeId': volume_id2, 'InstanceId': instance1_id, 'Device': '/dev/sdg'})
# attach_volume_id3 = creator('attach_volume', {'VolumeId': volume_id3, 'InstanceId': instance2_id, 'Device': '/dev/sde'})
#
# volume_from_snapshot_id1 = creator('volume_from_snapshot', {'SnapshotId': volume_snapshot1, 'AvailabilityZone': AvailabilityZone, 'VolumeType': 'standard', 'Encrypted': False})
# volume_from_snapshot_id2 = creator('volume_from_snapshot', {'SnapshotId': volume_snapshot2, 'AvailabilityZone': AvailabilityZone, 'VolumeType': 'standard', 'Encrypted': False})