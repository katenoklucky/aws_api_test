__author__ = 'edanilova'

from creator_script import creator
import json
from pprint import pprint


string = {
      "rank_id": 3,
      "instances": [
        {
          "ImageId": "ami-5189a661",
          "MinCount": 1,
          "MaxCount": 1,
          "KeyName": "my_key",
          "SecurityGroupIds": "sg-b04ed4d4",
          "InstanceType": "t2.micro",
          "SubnetId": "subnet-7cd4a80b"
        }
      ]
    }

with open('instances.json') as f:
    json_data = json.load(f)
    json_data["ranks"].append(string)
    with open('instances.json', 'w') as f:
        json.dump(json_data, f)
        pprint(json_data)
# pprint(json_data["ranks"][2])

# for i in json_data["vpcs"]:
#     vpc_id = creator('vpc', {'CidrBlock': i['CidrBlock']})
#
# for i in json_data["domains"]:
#     allocate_address = creator('allocate_address', {'Domain': i['Domain']})
#     public_ip = allocate_address[0]
#     allocation_id = allocate_address[1]
#
# for r in json_data["ranks"]:
#     for i in r["instances"]:
#         instance_id = creator('instance', {'ImageId': i['ImageId'], 'MinCount': i['MinCount'], 'MaxCount': i['MaxCount'], 'KeyName': i['KeyName'],
#                                      'SecurityGroupIds': i['SecurityGroupIds'], 'InstanceType': i['InstanceType'], 'SubnetId': i['SubnetId']})