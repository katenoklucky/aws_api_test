__author__ = 'edanilova'

import boto3
import time
import logging
from botocore import exceptions

# SERVICE_NAME = 'ec2'
# ACCESS_KEY_ID = r'AKIAIMQKWAW3UJ3SOJ2Q'
# SECRET_ACCESS_KEY = r'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'
# REGION_NAME = 'us-west-2'

ACCESS_KEY_ID = r'AKIAI7SWMXJILKHQ2WKA'
SECRET_ACCESS_KEY = r'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'
REGION_NAME = 'eu-central-1'

class AWSWrapper:
    def __init__(self, access_key_id, secret_access_key, region_name):
        # make session
        self.session = boto3.Session(aws_access_key_id=ACCESS_KEY_ID, aws_secret_access_key=SECRET_ACCESS_KEY, region_name=REGION_NAME)
        # # create client
        self.client = self.session.client('ec2', aws_access_key_id=ACCESS_KEY_ID, aws_secret_access_key=SECRET_ACCESS_KEY, region_name=REGION_NAME)
        # create ec2
        self.ec2 = self.session.resource('ec2')
        self.clientcloudformation = self.session.client('cloudformation', aws_access_key_id=ACCESS_KEY_ID, aws_secret_access_key=SECRET_ACCESS_KEY, region_name=REGION_NAME)
        self.cloudformation = self.session.resource('cloudformation')

class bcolors:
    Red = '\033[91m'
    Green = '\033[92m'
    Grey = '\033[90m'
    Magenta = '\033[95m'

aws = AWSWrapper(ACCESS_KEY_ID, SECRET_ACCESS_KEY, REGION_NAME)

def waiters(object, id):
    flag = True
    flag_print = False
    while flag:
        flag = False
        time.sleep(0.1)

        if object == 'key':
            process = 'deleted'
            key_pairs = aws.ec2.key_pairs.all()
            for item in key_pairs:
                if item.name == id:
                    flag = True

        elif object == 'volume_snapshot':
            process = 'deleted'
            volume_snapshots = aws.ec2.snapshots.all()
            for item in volume_snapshots:
                if item.id == id:
                    flag = True

        elif object == 'network_interfaces':
            process = 'deleted'
            network_interfaces = aws.ec2.network_interfaces.all()
            for item in network_interfaces:
                if item.id == id:
                    flag = True

        elif object == 'vpc':
            process = 'deleted'
            vpcs = aws.ec2.vpcs.all()
            for item in vpcs:
                if item.id == id:
                    flag = True

        elif object == 'subnet':
            process = 'deleted'
            subnets = aws.ec2.subnets.all()
            for item in subnets:
                if item.id == id:
                    flag = True

        elif object == 'route_table':
            process = 'deleted'
            route_tables = aws.ec2.route_tables.all()
            for item in route_tables:
                if item.id == id:
                    flag = True

        elif object == 'security_group':
            process = 'deleted'
            security_groups = aws.ec2.security_groups.all()
            for item in security_groups:
                if item.id == id:
                    flag = True

        elif object == 'attach_volume':
            process = 'detached'
            attach_volumes = aws.ec2.Volume(id).attachments
            for item in attach_volumes:
                if item.id == id:
                    flag = True

        elif object == 'volume':
            process = 'deleted'
            volumes = aws.ec2.volumes.all()
            for item in volumes:
                if item.id == id:
                    flag = True

        elif object == 'internet_gateway':
            process = 'deleted'
            internet_gateways = aws.ec2.internet_gateways.all()
            for item in internet_gateways:
                if item.id == id:
                    flag = True

        elif object == 'allocate_address':
            process = 'deleted'
            describe_addresses = aws.client.describe_addresses().get('Addresses')
            if describe_addresses is not None:
                for i in range(0, len(describe_addresses)):
                    for item in describe_addresses[i]:
                        try:
                            item.AllocationId
                            if item.AllocationId == id:
                                flag = True
                        except:
                            flag = False

        elif object == 'associate_address':
            process = 'disassociated'
            describe_addresses = aws.client.describe_addresses().get('Addresses')
            if describe_addresses is not None:
                for i in range(0, len(describe_addresses)):
                    for item in describe_addresses[i]:
                        try:
                            item.AssociationId
                            if item.AssociationId == id:
                                flag = True
                        except:
                            flag = False

        elif object == 'vpn_gateway':
            process = 'deleted'
            describe_vpn_gateways = aws.client.describe_vpn_gateways().get('VpnGateways')
            if describe_vpn_gateways is not None:
                for item in describe_vpn_gateways:
                    vpn_gateway = item.get('VpnGatewayId')
                    if vpn_gateway == id:
                        if item.get('State') == 'deleted':
                            flag = False

        elif object == 'customer_gateway':
            process = 'deleted'
            describe_customer_gateways = aws.client.describe_customer_gateways().get('CustomerGateways')
            if describe_customer_gateways is not None:
                for item in describe_customer_gateways:
                    customer_gateway = item.get('CustomerGatewayId')
                    if customer_gateway == id:
                        if item.get('State') == 'deleted':
                            break
                        else:
                            flag = True

        elif object == 'vpn_connection':
            process = 'deleted'
            describe_vpn_connections = aws.client.describe_vpn_connections()
            sdfsdf = describe_vpn_connections.get('VpnConnectionIds')
            if describe_vpn_connections is not None:
                for item in describe_vpn_connections:
                    if item == id:
                        flag = True

        elif object == 'attach_vpn_gateway':
            process = 'detached'
            describe_vpn_gateways = aws.client.describe_vpn_gateways().get('VpnGateways')
            if describe_vpn_gateways is not None:
                for item in describe_vpn_gateways:
                    attachments = item.get('VpcAttachments')
                    if attachments is not None:
                        if item.get('VpnGatewayId') == id:
                            for attachment in attachments:
                                if attachment.get('State') == 'detached':
                                    break
                                else:
                                    flag = True

        elif object == 'attached_vpn_gateway':
            process = 'attached'
            describe_vpn_gateways = aws.client.describe_vpn_gateways().get('VpnGateways')
            if describe_vpn_gateways is not None:
                for item in describe_vpn_gateways:
                    attachments = item.get('VpcAttachments')
                    if attachments is not None:
                        if item.get('VpnGatewayId') == id:
                            for attachment in attachments:
                                if attachment.get('State') == 'attached':
                                    break
                                else:
                                    flag = True

        if flag_print is False:
            print bcolors.Grey + 'Waiting while ' + object + ' ' + id + ' is ' + process + '...'
            flag_print = True

    return


def deleting_objects(object_name, parameters):
    try:
        if object_name == 'key':
            id = parameters[0]
            aws.ec2.KeyPair(id).delete()
            waiters(object_name, id)
            # wrapper.wait_while_keypair_deleted(id)

        elif object_name == 'instance':
            id = parameters[0]
            aws.ec2.Instance(id).terminate()
            print bcolors.Grey + 'Deleting instance ' + id + '...'
            waiter = aws.client.get_waiter('instance_terminated')
            waiter.wait(InstanceIds=[id])

        # elif object_name == 'instance_snapshot':
        #     aws.ec2.client.images.delete(id)
        #     wrapper.wait_while_instance_snapshot_deleted(id)

        elif object_name == 'vpc':
            id = parameters[0]
            waiter = aws.client.get_waiter('vpc_available')
            waiter.wait(VpcIds=[id])
            aws.client.delete_vpc(VpcId=id)

        elif object_name == 'attach_vpn_gateway':
            id = parameters[0]
            vpc_id = parameters[1]
            aws.client.detach_vpn_gateway(VpnGatewayId=id, VpcId=vpc_id)
            waiters(object_name, id)

        elif object_name == 'volume':
            id = parameters[0]
            aws.ec2.Volume(id).delete()
            # waiter = aws.client.get_waiter('volume_deleted')
            # waiter.wait(VolumeIds=[id])
            waiters(object_name, id)

        elif object_name == 'subnet':
            id = parameters[0]
            aws.ec2.Subnet(id).delete()
            waiters(object_name, id)

        elif object_name == 'route_table':
            id = parameters[0]
            aws.ec2.RouteTable(id).delete()
            waiters(object_name, id)

        elif object_name == 'route':
            id = parameters[0]
            destination_cidr_block = parameters[1]
            aws.client.delete_route(RouteTableId=id, DestinationCidrBlock=destination_cidr_block)

        # elif object_name == 'route_table_association':
        #     aws.ec2.RouteTableAssociation(id).delete()

        elif object_name == 'network_interface':
            id = parameters[0]
            aws.ec2.NetworkInterface(id).delete()
            waiters(object_name, id)

        elif object_name == 'volume_snapshot':
            id = parameters[0]
            aws.ec2.Snapshot(id).delete()
            waiters(object_name, id)

        elif object_name == 'attach_volume':
            id = parameters[0]
            aws.client.detach_volume(VolumeId=id)
            print bcolors.Grey + 'Detaching volume ' + id + '...'
            waiter = aws.client.get_waiter('volume_available')
            waiter.wait(VolumeIds=[id])

        elif object_name == 'security_group':
            id = parameters[0]
            aws.ec2.SecurityGroup(id).delete()
            waiters(object_name, id)

        elif object_name == 'internet_gateway':
            id = parameters[0]
            aws.ec2.InternetGateway(id).delete()
            waiters(object_name, id)

        elif object_name == 'internet_gateway_attach_to_vpc':
            id = parameters[0]
            vpc_id = parameters[1]
            aws.client.detach_internet_gateway(InternetGatewayId=id, VpcId=vpc_id)

        elif object_name == 'attach_network_interface':
            id = parameters[0]
            aws.client.detach_network_interface(AttachmentId=id)

        elif object_name == 'vpc_peering_connection':
            id = parameters[0]
            aws.client.delete_vpc_peering_connection(VpcPeeringConnectionId=id)

        elif object_name == 'associate_route_table':
            id = parameters[0]
            aws.client.disassociate_route_table(AssociationId=id)

        elif object_name == 'associate_address':
            id = parameters[0]
            aws.client.disassociate_address(AssociationId=id)
            waiters(object_name, id)

        elif object_name == 'allocate_address':
            id = parameters[0]
            time.sleep(1)
            aws.client.release_address(AllocationId=id)
            waiters(object_name, id)

        elif object_name == 'vpn_gateway':
            id = parameters[0]
            aws.client.delete_vpn_gateway(VpnGatewayId=id)
            waiters(object_name, id)

        elif object_name == 'customer_gateway':
            id = parameters[0]
            aws.client.delete_customer_gateway(CustomerGatewayId=id)
            waiters(object_name, id)

        elif object_name == 'vpn_connection':
            id = parameters[0]
            aws.client.delete_vpn_connection(VpnConnectionId=id)
            print bcolors.Grey + 'Waiting while vpn connection ' + id + ' is deleted...'
            waiter = aws.client.get_waiter('vpn_connection_deleted')
            waiter.wait(VpnConnectionIds=[id])

        elif object_name == 'vpn_connection_route':
            id = parameters[0]
            destination_cidr_block = parameters[1]
            aws.client.delete_vpn_connection_route(VpnConnectionId=id, DestinationCidrBlock=destination_cidr_block)

        else:
            print bcolors.Magenta + object_name.capitalize() + ' cannot be deleted because a function was not written.'

        print bcolors.Green + object_name.capitalize() + ' ' + id + ' was deleted.'
        return True

    except:
        print bcolors.Red + object_name.capitalize() + ' ' + id + ' was not deleted.'
        print logging.exception('')
        return False