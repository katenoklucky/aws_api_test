__author__ = 'edanilova'

from aws_creator import AWSWrapper
from aws_creator import bcolors
from aws_creator import waiters
import logging

# SERVICE_NAME = 'ec2'
# ACCESS_KEY_ID = r'AKIAIMQKWAW3UJ3SOJ2Q'
# SECRET_ACCESS_KEY = r'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'
# REGION_NAME = 'us-west-2'

ACCESS_KEY_ID = r'AKIAI7SWMXJILKHQ2WKA'
SECRET_ACCESS_KEY = r'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'
REGION_NAME = 'eu-central-1'


def write_to_file(text):
    file_for_delete = open('delete', 'a')
    file_for_delete.write(text + '\n')
    file_for_delete.close()


def add_tags(component, object):
    aws = AWSWrapper(ACCESS_KEY_ID, SECRET_ACCESS_KEY, REGION_NAME)
    aws.client.create_tags(Resources=[component],
                            Tags=[
                                {'Key': 'Name',
                                 'Value': 'ed_' + object}])


def creator(object, parameters):
    aws = AWSWrapper(ACCESS_KEY_ID, SECRET_ACCESS_KEY, REGION_NAME)
    try:
        # create key_pair if it not exists
        if object == 'key':
            key_name = parameters.get('KeyName')
            try:
                aws.client.describe_key_pairs(KeyNames=[key_name])
            except:
                key_pair = aws.ec2.create_key_pair(KeyName=key_name)
                print key_pair.__dict__
                key_name = key_pair.name
                print bcolors.Green + 'Key pair \'' + str(key_name) + '\' was successfully created.'
            write_to_file('10:key:' + key_name)
            return key_name



        elif object == 'vpc_peering_connection':
            vpc_id = parameters.get('VpcId')
            peer_vpc_id = parameters.get('PeerVpcId')
            vpc_peering_connection = aws.ec2.create_vpc_peering_connection(VpcId=vpc_id, PeerVpcId=peer_vpc_id)
            vpc_peering_connection_id = vpc_peering_connection.id
            add_tags(vpc_peering_connection_id, object)
            write_to_file('15:vpc_peering_connection:' + vpc_peering_connection_id)
            print bcolors.Green + 'VPC peering connection \'' + str(
                vpc_peering_connection_id) + '\' was successfully created.'
            return vpc_peering_connection_id



        elif object == 'attach_volume':
            volume_id = parameters.get('VolumeId')
            instance_id = parameters.get('InstanceId')
            device = parameters.get('Device')
            attach_volume = aws.client.attach_volume(VolumeId=volume_id, InstanceId=instance_id, Device=device)
            write_to_file('16:attach_volume:' + volume_id)
            print bcolors.Green + 'Volume \'' + str(volume_id) + '\' was successfully attached to \'' + str(
                instance_id) + '\'.'

        elif object == 'volume_from_snapshot':
            snapshot_id = parameters.get('SnapshotId')
            availability_zone = parameters.get('AvailabilityZone')
            print bcolors.Grey + 'Creating volume from snapshot...'
            volume_from_snapshot = aws.client.create_volume(SnapshotId=snapshot_id, AvailabilityZone=availability_zone)
            volume_from_snapshot_id = volume_from_snapshot.get('VolumeId')
            waiter = aws.client.get_waiter('volume_available')
            waiter.wait(VolumeIds=[volume_from_snapshot_id])
            add_tags(volume_from_snapshot_id, object)
            write_to_file('18:volume:' + volume_from_snapshot_id)
            print bcolors.Green + 'Volume \'' + str(
                volume_from_snapshot_id) + '\' from snapshot ' + snapshot_id + ' was successfully created.'
            return volume_from_snapshot_id

        elif object == 'volume_snapshot':
            volume_id = parameters.get('VolumeId')
            description = 'Volume snapshot for ' + volume_id
            print bcolors.Grey + 'Creating volume snapshot...'
            snapshot = aws.ec2.create_snapshot(VolumeId=volume_id, Description=description)
            snapshot_id = snapshot.id
            waiter = aws.client.get_waiter('snapshot_completed')
            waiter.wait(SnapshotIds=[snapshot_id])
            add_tags(snapshot_id, object)
            write_to_file('19:volume_snapshot:' + snapshot_id)
            print bcolors.Green + 'Snapshot \'' + str(snapshot_id) + '\' was successfully created.'
            return snapshot_id

        # create volume
        elif object == 'volume':
            size = parameters.get('Size')
            availability_zone = parameters.get('AvailabilityZone')
            volume_type = parameters.get('VolumeType')
            encrypted = parameters.get('Encrypted')
            print bcolors.Grey + 'Creating volume...'
            volume = aws.ec2.create_volume(Size=size, AvailabilityZone=availability_zone, VolumeType=volume_type,
                                            Encrypted=encrypted)
            volume_id = volume.id
            waiter = aws.client.get_waiter('volume_available')
            waiter.wait(VolumeIds=[volume_id])
            add_tags(volume_id, object)
            write_to_file('20:volume:' + volume_id)
            print bcolors.Green + 'Volume \'' + str(volume_id) + '\' was successfully created.'
            return volume_id

        # create instance
        elif object == 'instance':
            image_id = parameters.get('ImageId')
            min_count = parameters.get('MinCount')
            max_count = parameters.get('MaxCount')
            key_name = parameters.get('KeyName')
            security_group_ids =[parameters.get('SecurityGroupIds')]
            instance_type = parameters.get('InstanceType')
            subnet_id = parameters.get('SubnetId')
            # NetworkInterface = parameters('NetworkInterface')
            print bcolors.Grey + 'Creating instance...'
            instances = aws.ec2.create_instances(ImageId=image_id, MinCount=min_count, MaxCount=max_count,
                                                  KeyName=key_name, SecurityGroupIds=security_group_ids,
                                                  InstanceType=instance_type, SubnetId=subnet_id)
            instance_ids =[]
            for i in range(0, len(instances)):
                instance_id = instances[i].id
                instance_ids.append(instance_id)
                waiter = aws.client.get_waiter('instance_running')
                waiter.wait(InstanceIds=[instance_id])
                add_tags(instance_id, object)
                write_to_file('40:instance:' + instance_id)
                print bcolors.Green + 'Instance \'' + str(instance_id) + '\' was successfully created.'
            return instance_ids

        elif object == 'allocate_address':
            result =[]
            domain = parameters.get('Domain')
            allocate_address = aws.client.allocate_address(Domain=domain)
            public_ip = allocate_address.get('PublicIp')
            allocation_id = allocate_address.get('AllocationId')
            result.append(public_ip)
            result.append(allocation_id)
            write_to_file('69:allocate_address:' + allocation_id)
            print bcolors.Green + 'Elastic IP address \'' + str(
                public_ip) + '\' was successfully acquired. Allocation Id is ' + allocation_id + '.'
            return result

        elif object == 'internet_gateway_attach_to_vpc':
            internet_gateway_id = parameters.get('Internet_Gateway_ID')
            vpc_id = parameters.get('VpcId')
            aws.client.attach_internet_gateway(InternetGatewayId=internet_gateway_id, VpcId=vpc_id)
            write_to_file('70:internet_gateway_attach_to_vpc:' + internet_gateway_id + ',' + vpc_id)
            print bcolors.Green + 'Internet gateway \'' + str(
                internet_gateway_id) + '\' was successfully attached to network ' + vpc_id + '.'

        elif object == 'internet_gateway':
            internet_gateway = aws.ec2.create_internet_gateway()
            internet_gateway_id = internet_gateway.id
            add_tags(internet_gateway_id, object)
            write_to_file('71:internet_gateway:' + internet_gateway_id)
            print bcolors.Green + 'Internet gateway \'' + str(internet_gateway_id) + '\' was successfully created.'
            return internet_gateway_id

        elif object == 'route':
            route_table_id = parameters.get('RouteTableId')
            destination_cidr_block = parameters.get('DestinationCidrBlock')
            gateway_id = parameters.get('GatewayId')
            instance_id = parameters.get('InstanceId')
            network_interface_id = parameters.get('NetworkInterfaceId')
            vpc_peering_connection_id = parameters.get('VpcPeeringConnectionId')
            route = None
            if instance_id is not None:
                route = aws.client.create_route(RouteTableId=route_table_id,
                                                 DestinationCidrBlock=destination_cidr_block, InstanceId=instance_id)
            elif gateway_id is not None:
                route = aws.client.create_route(RouteTableId=route_table_id,
                                                 DestinationCidrBlock=destination_cidr_block, GatewayId=gateway_id)
            elif network_interface_id is not None:
                route = aws.client.create_route(RouteTableId=route_table_id,
                                                 DestinationCidrBlock=destination_cidr_block,
                                                 NetworkInterfaceId=network_interface_id)
            elif vpc_peering_connection_id is not None:
                route = aws.client.create_route(RouteTableId=route_table_id,
                                                 DestinationCidrBlock=destination_cidr_block,
                                                 VpcPeeringConnectionId=vpc_peering_connection_id)
            # write_to_file('72:route:' + RouteTableId + ',' + DestinationCidrBlock)
            print bcolors.Green + 'Route in route table \'' + str(route_table_id) + '\' was successfully created.'

        elif object == 'associate_route_table':
            subnet_id = parameters.get('SubnetId')
            route_table_id = parameters.get('RouteTableId')
            associate_route_table = aws.client.associate_route_table(SubnetId=subnet_id, RouteTableId=route_table_id)
            associate_route_table_id = associate_route_table.get('AssociationId')
            write_to_file('73:associate_route_table:' + associate_route_table_id)
            print bcolors.Green + 'Subnet \'' + str(
                subnet_id) + '\' was successfully associated with route table ' + str(route_table_id) + '.'
            return associate_route_table_id

        elif object == 'route_table':
            vpc_id = parameters.get('VpcId')
            route_table = aws.client.create_route_table(VpcId=vpc_id)
            route_table_id = route_table.get('RouteTable').get('RouteTableId')
            add_tags(route_table_id, object)
            write_to_file('74:route_table:' + route_table_id)
            print bcolors.Green + 'Route table \'' + str(route_table_id) + '\' was successfully created.'
            return route_table_id

        elif object == 'attach_network_interface':
            network_interface_id = parameters.get('NetworkInterfaceId')
            instance_id = parameters.get('InstanceId')
            device_index = parameters.get('DeviceIndex')
            attach_network_interface = aws.client.attach_network_interface(NetworkInterfaceId=network_interface_id,
                                                                            InstanceId=instance_id,
                                                                            DeviceIndex=device_index)
            attach_network_interface_id = attach_network_interface.get('AttachmentId')
            # write_to_file('75:attach_network_interface:' + attach_network_interface_id)
            print bcolors.Green + 'Network interface \'' + str(
                network_interface_id) + '\' was successfully attached to instance ' + instance_id + '.'
            return attach_network_interface_id

        elif object == 'network_interface':
            subnet_id = parameters.get('SubnetId')
            network_interface = aws.ec2.create_network_interface(SubnetId=subnet_id)
            network_interface_id = network_interface.id
            add_tags(network_interface_id, object)
            write_to_file('77:network_interface:' + network_interface_id)
            print bcolors.Green + 'Network interface \'' + str(network_interface_id) + '\' was successfully created.'
            return network_interface_id

        elif object == 'associate_address':
            instance_id = parameters.get('InstanceId')
            allocation_id = parameters.get('AllocationId')
            network_interface_id = parameters.get('NetworkInterfaceId')
            # PrivateIpAddress = parameters.get('PrivateIpAddress')
            associate_address = None
            if instance_id is not None:
                associate_address = aws.client.associate_address(InstanceId=instance_id, AllocationId=allocation_id)
                print bcolors.Green + 'Allocated address \'' + str(
                    allocation_id) + '\' was successfully associated with instance ' + instance_id + '.'
            if network_interface_id is not None:
                associate_address = aws.client.associate_address(NetworkInterfaceId=network_interface_id,
                                                                  AllocationId=allocation_id)
                print bcolors.Green + 'Allocated Elastic IP address \'' + str(
                    allocation_id) + '\' was successfully associated with Network Interface Id ' + network_interface_id + '.'
            associate_address_id = associate_address.get('AssociationId')
            # write_to_file('78:associate_address:' + associate_address_id)
            return associate_address_id

        elif object == 'subnet':
            vpc_id = parameters.get('VpcId')
            cidr_block = parameters.get('CidrBlock')
            availability_zone = parameters.get('AvailabilityZone')
            subnet = aws.ec2.create_subnet(VpcId=vpc_id, CidrBlock=cidr_block, AvailabilityZone=availability_zone)
            subnet_id = subnet.id
            add_tags(subnet_id, object)
            write_to_file('80:subnet:' + subnet_id)
            print bcolors.Green + 'Subnet \'' + str(subnet_id) + '\' with CidrBlock ' + cidr_block + ' was ' \
                                                                                                      'successfully created.'
            return subnet_id

        elif object == 'authorize_security_group_egress':
            group_id = parameters.get('GroupId')
            ip_permissions = parameters.get('IpPermissions')
            authorize_security_group_egress = aws.client.authorize_security_group_egress(GroupId=group_id,
                                                                                          IpPermissions=ip_permissions)
            print bcolors.Green + 'Egress rules \'' + str(ip_permissions) + '\' was successfully added to security ' \
                                                                             'group ' + group_id + '.'
            return authorize_security_group_egress

        elif object == 'authorize_security_group_ingress':
            group_id = parameters.get('GroupId')
            ip_permissions = parameters.get('IpPermissions')
            authorize_security_group_ingress = aws.client.authorize_security_group_ingress(GroupId=group_id,
                                                                                            IpPermissions=ip_permissions)
            print bcolors.Green + 'Ingress rules \'' + str(ip_permissions) + '\' was successfully added ' \
                                                                              'to security group ' + group_id + '.'
            return authorize_security_group_ingress

        elif object == 'security_group':
            group_name = parameters.get('GroupName')
            description = parameters.get('Description')
            vpc_id = parameters.get('VpcId')
            security_group = aws.ec2.create_security_group(GroupName=group_name, Description=description, VpcId=vpc_id)
            security_group_id = security_group.id
            add_tags(security_group_id, object)
            write_to_file('81:security_group:' + security_group_id)
            print bcolors.Green + 'Security group \'' + str(security_group_id) + '\' was successfully created.'
            return security_group_id

        elif object == 'vpn_connection_route':
            vpn_connection_id = parameters.get('VpnConnectionId')
            destination_cidr_block = parameters.get('DestinationCidrBlock')
            vpn_connection_route = aws.client.create_vpn_connection_route(VpnConnectionId=vpn_connection_id,
                                                                           DestinationCidrBlock=destination_cidr_block)
            write_to_file('82:vpn_connection_route:' + vpn_connection_id + ',' + destination_cidr_block)
            print bcolors.Green + 'Vpn connection route ' + str(vpn_connection_route) + ' in Vpn connection \'' + \
                  str(vpn_connection_id) + '\' with DestinationCidrBlock ' + str(destination_cidr_block) + \
                  ' was successfully created.'
            return vpn_connection_route

        elif object == 'vpn_connection':
            type = parameters.get('Type')
            customer_gateway_id = parameters.get('CustomerGatewayId')
            vpn_gateway_id = parameters.get('VpnGatewayId')
            options = parameters.get('Options')
            print bcolors.Grey + 'Creating VPN connection...'
            vpn_connection = aws.client.create_vpn_connection(Type=type, CustomerGatewayId=customer_gateway_id,
                                                               VpnGatewayId=vpn_gateway_id, Options=options)
            vpn_connection_id = vpn_connection.get('VpnConnection').get('VpnConnectionId')
            customer_gateway_configuration = vpn_connection.get('VpnConnection').get('CustomerGatewayConfiguration')
            waiter = aws.client.get_waiter('vpn_connection_available')
            waiter.wait(VpnConnectionIds=[vpn_connection_id])
            add_tags(vpn_connection_id, object)
            write_to_file('84:vpn_connection:' + vpn_connection_id)
            print bcolors.Green + 'Vpn connection \'' + str(vpn_connection_id) + '\' was successfully created.'
            print 'Customer gataway configuration \n:' + customer_gateway_configuration
            return vpn_connection_id

        elif object == 'attach_vpn_gateway':
            vpn_gateway_id = parameters.get('VpnGatewayId')
            vpc_id = parameters.get('VpcId')
            aws.client.attach_vpn_gateway(VpnGatewayId=vpn_gateway_id, VpcId=vpc_id)
            waiters('attached_vpn_gateway', vpn_gateway_id)
            write_to_file('85:attach_vpn_gateway:' + vpn_gateway_id + ',' + vpc_id)
            print bcolors.Green + 'VPN gateway \'' + str(
                vpn_gateway_id) + '\' was successfully attached with network ' + str(vpc_id) + '.'
            return vpn_gateway_id

        elif object == 'customer_gateway':
            type = parameters.get('Type')
            public_ip = parameters.get('PublicIp')
            bgp_asn = parameters.get('BgpAsn')
            customer_gateway = aws.client.create_customer_gateway(Type=type, PublicIp=public_ip, BgpAsn=bgp_asn)
            customer_gateway_id = customer_gateway.get('CustomerGateway').get('CustomerGatewayId')
            waiter = aws.client.get_waiter('customer_gateway_available')
            waiter.wait(CustomerGatewayIds=[customer_gateway_id])
            add_tags(customer_gateway_id, object)
            write_to_file('86:customer_gateway:' + customer_gateway_id)
            print bcolors.Green + 'Customer gateway \'' + str(customer_gateway_id) + '\' was successfully created.'
            return customer_gateway_id

        elif object == 'vpn_gateway':
            type = parameters.get('Type')
            availability_zone = parameters.get('AvailabilityZone')
            vpn_gateway = aws.client.create_vpn_gateway(Type=type, AvailabilityZone=availability_zone)
            vpn_gateway_id = vpn_gateway.get('VpnGateway').get('VpnGatewayId')
            add_tags(vpn_gateway_id, object)
            write_to_file('87:vpn_gateway:' + vpn_gateway_id)
            print bcolors.Green + 'VPN gateway \'' + str(vpn_gateway_id) + '\' was successfully created.'
            return vpn_gateway_id

        elif object == 'vpc':
            cidr_block = parameters.get('CidrBlock')
            vpc = aws.client.create_vpc(CidrBlock=cidr_block)
            vpc_id = vpc.get('Vpc').get('VpcId')
            waiter = aws.client.get_waiter('vpc_available')
            waiter.wait(VpcIds=[vpc_id])
            add_tags(vpc_id, object)
            write_to_file('90:vpc:' + vpc_id)
            print bcolors.Green + 'Network \'' + str(vpc_id) + '\' was successfully created.'
            aws.client.modify_vpc_attribute(VpcId=vpc_id, EnableDnsHostnames={'Value': True})
            print bcolors.Green + 'DnsHostnames for network \'' + str(vpc_id) + '\' was successfully enabled.'
            return vpc_id

        elif object == 'get_main_route_table':
            vpc_id = parameters.get('vpc_id')
            all = aws.client.describe_route_tables(Filters=[{
                'Name': 'vpc-id',
                'Values':[vpc_id]
            }])
            route_tables = all.get('RouteTables')
            main_rt_id = None
            for i in range(0, len(route_tables)):
                associations = route_tables[i].get('Associations')
                for k in range(len(associations)):
                    if associations[k].get('Main') is True:
                        main_rt_id = route_tables[k].get('RouteTableId')
                        break
            print 'Main route table is ' + main_rt_id + ' in network ' + vpc_id + '.'
            return main_rt_id

        elif object == 'modify_instance_attribute_source_dest_check':
            instance_id = parameters.get('InstanceId')
            value = parameters.get('Value')
            aws.client.modify_instance_attribute(InstanceId=instance_id, SourceDestCheck={'Value': value})
            print 'Attribute SourceDestCheck with value ' + str(value) + ' was successfully modified for instance ' + str(instance_id) + '.'
            return True

    except:
        print bcolors.Red + object.capitalize() + ' was not created.'
        print logging.exception('')
