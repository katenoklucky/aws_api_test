__author__ = 'edanilova'

from aws_creator import deleting_objects

def delete_objects():
    linelist = []
    # newlist = []
    file = open('delete', 'r')
    for line in file:
        if line != '':
            linelist.append(line)
    file.close()
    linelist.sort()
    file = open('delete', 'w')
    for list_items in linelist:
        file.write(list_items)
    file.close()
    file = open('delete', 'r')
    i = 0
    while len(linelist) > 0:
        if i < len(linelist):
            line = linelist[i]
            object = line.rstrip('\n')
            line_list = object.split(':')
            priority = line_list[0]
            object_name = line_list[1]
            parameters = line_list[2]
            parameters_list = None
            try:
                parameters.index(',')
                parameters_list = parameters.split(',')
            except:
                parameters_list = [parameters]
            if deleting_objects(object_name, parameters_list) == True:
                linelist.remove(line)
                file.close()
                file = open('delete', 'w')
                for list_items in linelist:
                    file.write(list_items)
                file.close()
                file = open('delete', 'r')
            else:
                i = i + 1

    file = open('delete', 'w')
    file.write('')
    file.close()

if __name__ == '__main__':
    delete_objects()