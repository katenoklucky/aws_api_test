__author__ = 'edanilova'

# http://f1.s.qip.ru/yVu0pxbD.png

from creator_script import creator

# for VPC main
# CidrBlock1 = '172.33.0.0/16'
CidrBlock1 = '10.0.0.0/16'
# for main subnet
CidrBlock2 = '10.0.0.0/24'
# for VPN destination
CidrBlock3 = '10.0.1.0/16'
CidrBlock4 = '10.0.1.0/24'

# CidrBlock4 = '172.35.0.0/16'

AvailabilityZone = 'eu-central-1a'
AvailabilityZone1 = 'eu-central-1b'
# vpc_id1 = 'vpc-4bbff42e'

key_name = 'ed_key'
ami = 'ami-accff2b1'

# vpc_main = 'vpc-490db520'
# vpc_destination = 'vpc-480db521'
# main_rt1 = 'rtb-6676f70f'
# main_rt2 = 'rtb-7976f710'

vpc_id = creator('vpc', {'CidrBlock': CidrBlock1})
main_rt1 = creator('get_main_route_table', {'vpc_id': vpc_id})

allocate_address_i1 = creator('allocate_address', {'Domain': 'vpc'})
public_ip_i1 = allocate_address_i1[0]
allocation_id_i1 = allocate_address_i1[1]

subnet_id1 = creator('subnet', {'VpcId': vpc_id, 'CidrBlock': CidrBlock2, 'AvailabilityZone': AvailabilityZone})
subnet_id2 = creator('subnet', {'VpcId': vpc_id, 'CidrBlock': CidrBlock4, 'AvailabilityZone': AvailabilityZone})

internet_gateway_id1 = creator('internet_gateway', {})

internet_gateway_attach_to_vpc1 = creator('internet_gateway_attach_to_vpc', {'Internet_Gateway_ID': internet_gateway_id1, 'VpcId': vpc_id})

route_table_id1 = creator('route_table', {'VpcId': vpc_id})

route1 = creator('route', {'RouteTableId': route_table_id1, 'DestinationCidrBlock': '0.0.0.0/0', 'GatewayId': internet_gateway_id1})
associate_route_table_id_i1 = creator('associate_route_table', {'SubnetId': subnet_id1, 'RouteTableId': route_table_id1})

security_group_NAT_id = creator('security_group', {'GroupName': 'Security Group NAT Test', 'Description': 'Description', 'VpcId': vpc_id})


authorize_security_group_ingress_tcp1 = creator('authorize_security_group_ingress', {'GroupId': security_group_NAT_id,
                                                                    'IpPermissions': [{'IpProtocol': 'tcp',
                                                                    'FromPort': 80,
                                                                    'ToPort': 80,
                                                                    'IpRanges': [{'CidrIp': CidrBlock4}]}]})

authorize_security_group_ingress_tcp2 = creator('authorize_security_group_ingress', {'GroupId': security_group_NAT_id,
                                                                    'IpPermissions': [{'IpProtocol': 'tcp',
                                                                    'FromPort': 443,
                                                                    'ToPort': 443,
                                                                    'IpRanges': [{'CidrIp': CidrBlock4}]}]})

authorize_security_group_ingress_tcp3 = creator('authorize_security_group_ingress', {'GroupId': security_group_NAT_id,
                                                                    'IpPermissions': [{'IpProtocol': 'tcp',
                                                                    'FromPort': 22,
                                                                    'ToPort': 22,
                                                                    'IpRanges': [{'CidrIp': '37.44.40.66/32'}]}]})

authorize_security_group_ingress_tcp = creator('authorize_security_group_ingress', {'GroupId': security_group_NAT_id,
                                                                    'IpPermissions': [{'IpProtocol': 'tcp',
                                                                    'FromPort': 22,
                                                                    'ToPort': 22,
                                                                    'IpRanges': [{'CidrIp': '0.0.0.0/0'}]}]})

authorize_security_group_ingress_icmp = creator('authorize_security_group_ingress', {'GroupId': security_group_NAT_id,
                                                                    'IpPermissions': [{'IpProtocol': 'icmp',
                                                                    'FromPort': -1,
                                                                    'ToPort': -1,
                                                                    'IpRanges': [{'CidrIp': CidrBlock1}]}]})

authorize_security_group_egress_tcp = creator('authorize_security_group_egress', {'GroupId': security_group_NAT_id,
                                                                    'IpPermissions': [{'IpProtocol': 'tcp',
                                                                    'FromPort': 80,
                                                                    'ToPort': 80,
                                                                    'IpRanges': [{'CidrIp': '0.0.0.0/0'}]}]})

authorize_security_group_egress_tcp = creator('authorize_security_group_egress', {'GroupId': security_group_NAT_id,
                                                                    'IpPermissions': [{'IpProtocol': 'tcp',
                                                                    'FromPort': 443,
                                                                    'ToPort': 443,
                                                                    'IpRanges': [{'CidrIp': '0.0.0.0/0'}]}]})

authorize_security_group_egress_tcp = creator('authorize_security_group_egress', {'GroupId': security_group_NAT_id,
                                                                    'IpPermissions': [{'IpProtocol': 'tcp',
                                                                    'FromPort': 22,
                                                                    'ToPort': 22,
                                                                    'IpRanges': [{'CidrIp': '0.0.0.0/0'}]}]})

security_group_id = creator('security_group', {'GroupName': 'Security Group Test', 'Description': 'Description', 'VpcId': vpc_id})

authorize_security_group_ingress_tcp = creator('authorize_security_group_ingress', {'GroupId': security_group_id,
                                                                    'IpPermissions': [{'IpProtocol': 'tcp',
                                                                    'FromPort': 22,
                                                                    'ToPort': 22,
                                                                    'IpRanges': [{'CidrIp': '0.0.0.0/0'}]}]})

authorize_security_group_egress_tcp = creator('authorize_security_group_egress', {'GroupId': security_group_id,
                                                                    'IpPermissions': [{'IpProtocol': 'tcp',
                                                                    'FromPort': 22,
                                                                    'ToPort': 22,
                                                                    'IpRanges': [{'CidrIp': '0.0.0.0/0'}]}]})

instance_id1 = creator('instance', {'ImageId': 'ami-204c7a3d', 'MinCount': 1, 'MaxCount': 1, 'KeyName': key_name,
                                     'SecurityGroupIds': security_group_NAT_id, 'InstanceType': 't2.micro', 'SubnetId': subnet_id1})

creator('modify_instance_attribute_source_dest_check', {'InstanceId': instance_id1[0], 'Value': False})
associate_address_to_instance_id = creator('associate_address', {'InstanceId': instance_id1[0], 'AllocationId': allocation_id_i1, 'NetworkInterfaceId': None})

associate_route_table_id_i2 = creator('associate_route_table', {'SubnetId': subnet_id2, 'RouteTableId': main_rt1})

instance_id2 = creator('instance', {'ImageId': ami, 'MinCount': 1, 'MaxCount': 1, 'KeyName': key_name,
                                    'SecurityGroupIds': security_group_id, 'InstanceType': 't2.micro', 'SubnetId': subnet_id2})
route2 = creator('route', {'RouteTableId': main_rt1, 'DestinationCidrBlock': '0.0.0.0/0', 'InstanceId': instance_id1[0]})